import React, {useState, useEffect} from 'react'
import CourseCard from '../components/CourseCard'
import courseData from '../data/courses'
import EmptyData from '../components/EmptyData'
import {Row} from 'react-bootstrap'

export default function Courses(){
	// console.log(courseData)
	// const [firstName, setFirstName] = useState("Juan"); //useState() receives initial value
	//getter and setter
    // console.log(firstName)
    
	const [courses, setCourses] = useState([]);

    useEffect(()=>{
        setCourses(courseData)
    }, [])

	let courseList; //declaration of variable

	if (courses.length != 0) {
		courseList = courses.map((individualCourse)=>{
				return <CourseCard course={individualCourse} /> //passing props called course with a value indivudualCourse to CourseCard component
		})
	} else {
		courseList = <EmptyData/>
	}


	return(
		<>
			{courseList}
			{/*<button onClick={()=> setFirstName("Pedro") }>Click Me to change firstName value</button>*/}
		</>
		)
}

/*

	Courses passed down a props called course to CourseCard

	CourseCard = {
		props: {course: id, name, description, price, onOffer}
	}

	{course: {id,name, description, price, onOffer}}

*/