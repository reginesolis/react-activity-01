import React, {useState, useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'

export default function Register(){

	const [email, setEmail] = useState('')
	const [username, setUsername] = useState('')
	const [password, setPassword] = useState('')
	const [cfmPassword, setCfmPassword] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)
	const register = (e) => {
		e.preventDefault()
		alert(`You are successfully registered with below account details:
				email: ${email}
				username: ${username}
				`)

		setEmail('')
		setUsername('')
		setPassword('')
		setCfmPassword('')
	}

	// create a data validation that will check if the input fields are not empty and password and confirm password is matched
	useEffect(()=>{

		// if (typeof email !== 'undefined' && typeof username !== 'undefined' && typeof password !== 'undefined' && typeof cfmPassword !== 'undefined'){
		// 	setIsDisabled(false)
		// }

		if((email !== '' && username !== '' && password !== '' && cfmPassword !== '') && (password === cfmPassword)){
			setIsDisabled(false)
		}

	},[email, username, password, cfmPassword])


	return(
		<>
		<p className="text-center display-4 mt-5">Registration</p>
		<Form className="mt-5" onSubmit={(e)=> register(e)}>

		  <Form.Group controlId="formBasicEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control type="email" placeholder="Enter email" 
		    	onChange={(e)=> setEmail(e.target.value)} 
		    	value={email}
		    />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group controlId="formBasicUserName">
		    <Form.Label>User name</Form.Label>
		    <Form.Control type="text" placeholder="Enter username" 
		    	onChange={(e)=> setUsername(e.target.value)} 
		    	value={username}
		    />
		  </Form.Group>

		  <Form.Group controlId="formBasicPassword">
		    <Form.Label>Password</Form.Label>
		    <Form.Control type="password" placeholder="Password" 
		    	onChange={e => setPassword(e.target.value)}
		    	value={password}
		    />
		  </Form.Group>

		  <Form.Group controlId="formBasicPassword">
		    <Form.Label>Confirm password</Form.Label>
		    <Form.Control type="password" placeholder="Confirm your password" 
		    	onChange={e => setCfmPassword(e.target.value)}
		    	value={cfmPassword}
		    />
		  </Form.Group>

		  <Button variant="primary" type="submit" disabled={isDisabled}>
		    Submit
		  </Button>
		</Form>
		</>
		)
}