import React, {useState, useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'

export default function Login(){

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)
	const login = (e) => {
		e.preventDefault()
		alert(`You are successfully logged in on our app!`)

		setEmail('')
		setPassword('')
	}

	// create a data validation that will check if the input fields are not empty and password and confirm password is matched
	useEffect(()=>{

		// if (typeof email !== 'undefined' && typeof username !== 'undefined' && typeof password !== 'undefined' && typeof cfmPassword !== 'undefined'){
		// 	setIsDisabled(false)
		// }

		if(email !== '' && password !== ''){
			setIsDisabled(false)
		}

	},[email, password])


	return(
		<>
		<p className="text-center display-4 mt-5">Log In</p>
		<Form className="mt-5" onSubmit={(e)=> login(e)}>

		  <Form.Group controlId="formBasicEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control type="email" placeholder="Enter email" 
		    	onChange={(e)=> setEmail(e.target.value)} 
		    	value={email}
		    />
		  </Form.Group>

		  <Form.Group controlId="formBasicPassword">
		    <Form.Label>Password</Form.Label>
		    <Form.Control type="password" placeholder="Password" 
		    	onChange={e => setPassword(e.target.value)}
		    	value={password}
		    />
		  </Form.Group>

		  <Button variant="primary" type="submit" disabled={isDisabled}>
		    Submit
		  </Button>
		</Form>
		</>
		)
}