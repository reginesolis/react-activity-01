import React from 'react'; //import React Object from react package
import {Container} from 'react-bootstrap'
import AppNavBar from './components/AppNavBar'
import Banner from './components/Banner'
import LandingPage from './pages/LandingPage'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'


function App(){
  return(
    <>
      <AppNavBar/>
      <Container>
        <Login/>
      </Container>
    </>
  )
}

export default App