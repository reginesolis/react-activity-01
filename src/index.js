import React from 'react';
import ReactDOM from 'react-dom'; //manipulates virtual dom
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(<App/> , document.getElementById('root'));
//responsible for rendering components to our browser