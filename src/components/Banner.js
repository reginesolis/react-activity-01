import React from 'react'
import {Jumbotron, Button, Card, CardGroup, Row, Col} from 'react-bootstrap'

export default function Banner(){
    return(
        <>
        <Jumbotron style={{ margin: '5rem'}}>
            <h1>Zuitt Coding Bootcamp</h1>
            <p>
            Opportunities for everyone, everywhere
            </p>
            <p>
            <Button variant="primary">Enroll now!</Button>
            </p>
        </Jumbotron>

        <Row>
        <Col>
            <CardGroup>
                <Card className="mx-5" style={{ width: '20rem' }}>
                <Card.Body>
                    <Card.Title><h2>Learn from Home</h2></Card.Title>
                    <Card.Text>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </Card.Text>
                </Card.Body>
                </Card>

                <Card style={{ width: '20rem' }}>
                <Card.Body>
                    <Card.Title><h2>Study Now, Pay Later</h2></Card.Title>
                    <Card.Text>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </Card.Text>
                </Card.Body>
                </Card>

                <Card className="mx-5" style={{ width: '20rem' }}>
                <Card.Body>
                    <Card.Title><h2>Be Part of Our Community</h2></Card.Title>
                    <Card.Text>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </Card.Text>
                </Card.Body>
                </Card>
            </CardGroup>
        </Col>
    </Row>
        </>
    )
}
